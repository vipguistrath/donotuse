-- users table structure
CREATE TABLE IF NOT EXISTS users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255),
  password VARCHAR(255),
  created datetime,
  updated datetime
);

-- uploads table structure
CREATE TABLE IF NOT EXISTS uploads (
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  name VARCHAR(50),
  created datetime,
  updated datetime,
  FOREIGN KEY user_key (user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS data (
  id INT AUTO_INCREMENT PRIMARY KEY,
  upload_ud INT NOT NULL,
  log_time datetime NOT NULL,
  sensor_0 decimal(4,2),
  sensor_1 decimal(4,2),
  sensor_2 decimal(4,2),
  sensor_3 decimal(4,2),
  sensor_4 decimal(4,2),
  sensor_5 decimal(4,2),
  sensor_6 decimal(4,2),
  sensor_7 decimal(4,2),
  sensor_8 decimal(4,2),
  sensor_9 decimal(4,2),
  sensor_10 decimal(4,2),
  sensor_11 decimal(4,2),
  status_0 tinyint(3) unsigned,
  status_1 tinyint(3) unsigned,
  FOREIGN KEY upload_key (upload_id) REFERENCES uploads(id)
);
