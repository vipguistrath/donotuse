<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Upload'), ['action' => 'edit', $upload->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Upload'), ['action' => 'delete', $upload->id], ['confirm' => __('Are you sure you want to delete # {0}?', $upload->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Uploads'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Upload'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="uploads view large-10 medium-9 columns">
    <h2><?= h($upload->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $upload->has('user') ? $this->Html->link($upload->user->id, ['controller' => 'Users', 'action' => 'view', $upload->user->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($upload->name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($upload->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($upload->created) ?></p>
            <h6 class="subheader"><?= __('Updated') ?></h6>
            <p><?= h($upload->updated) ?></p>
        </div>
    </div>
</div>
