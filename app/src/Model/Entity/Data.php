<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Data Entity.
 */
class Data extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'upload_id' => true,
        'log_time' => true,
        'sensor_0' => true,
        'sensor_1' => true,
        'sensor_2' => true,
        'sensor_3' => true,
        'sensor_4' => true,
        'sensor_5' => true,
        'sensor_6' => true,
        'sensor_7' => true,
        'sensor_8' => true,
        'sensor_9' => true,
        'sensor_10' => true,
        'sensor_11' => true,
        'status_0' => true,
        'status_1' => true,
        'upload' => true,
    ];
}
