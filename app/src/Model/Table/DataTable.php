<?php
namespace App\Model\Table;

use App\Model\Entity\Data;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Data Model
 */
class DataTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('data');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Uploads', [
            'foreignKey' => 'upload_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('upload_id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('upload_id', 'create')
            ->notEmpty('upload_id')
            ->add('log_time', 'valid', ['rule' => 'datetime'])
            ->requirePresence('log_time', 'create')
            ->notEmpty('log_time')
            ->add('sensor_0', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_0')
            ->add('sensor_1', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_1')
            ->add('sensor_2', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_2')
            ->add('sensor_3', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_3')
            ->add('sensor_4', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_4')
            ->add('sensor_5', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_5')
            ->add('sensor_6', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_6')
            ->add('sensor_7', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_7')
            ->add('sensor_8', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_8')
            ->add('sensor_9', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_9')
            ->add('sensor_10', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_10')
            ->add('sensor_11', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('sensor_11')
            ->add('status_0', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('status_0')
            ->add('status_1', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('status_1');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['upload_id'], 'Uploads'));
        return $rules;
    }
}
