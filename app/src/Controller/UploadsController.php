<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Uploads Controller
 *
 * @property \App\Model\Table\UploadsTable $Uploads
 */
class UploadsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $this->set('uploads', $this->paginate($this->Uploads));
        $this->set('_serialize', ['uploads']);
    }

    /**
     * View method
     *
     * @param string|null $id Upload id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $upload = $this->Uploads->get($id, [
            'contain' => ['Users']
        ]);
        $this->set('upload', $upload);
        $this->set('_serialize', ['upload']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $upload = $this->Uploads->newEntity();

        if ($this->request->is('post')) {
            // Put filename into row
            $filename = strstr($this->request->data['submittedfile']['name'], '.', TRUE);
            $data = ['name' => $filename];
            $upload = $this->Uploads->newEntity($data);

            $upload = $this->Uploads->patchEntity($upload, $this->request->data);
            if ($this->Uploads->save($upload)) {
                if ($this->uploadFile()) {
                    $this->Flash->success('The file has been saved.');
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error('Failed to import CSV file');
                }
            } else {
                $this->Flash->error('The file could not be saved. Please, try again.');
            }
        }
        $users = $this->Uploads->Users->find('list', ['limit' => 200]);
        $this->set(compact('upload', 'users'));
        $this->set('_serialize', ['upload']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Upload id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $upload = $this->Uploads->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $upload = $this->Uploads->patchEntity($upload, $this->request->data);
            if ($this->Uploads->save($upload)) {
                $this->Flash->success('The upload has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The upload could not be saved. Please, try again.');
            }
        }
        $users = $this->Uploads->Users->find('list', ['limit' => 200]);
        $this->set(compact('upload', 'users'));
        $this->set('_serialize', ['upload']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Upload id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $upload = $this->Uploads->get($id);
        if ($this->Uploads->delete($upload)) {
            $this->Flash->success('The upload has been deleted.');
        } else {
            $this->Flash->error('The upload could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }

    public function uploadFile()
    {
        $this->loadModel('Data');

        $filename = $this->request->data['submittedfile']['tmp_name'];
        $handle = fopen($filename, 'r');

        // Get headers
        $header = fgetcsv($handle);

        // Get data
        while (($row = fgetcsv($handle)) !== FALSE) {
            $entity = $this->Data->newEntity();
            $data = [
                'log_time' => $row[0],
                'sensor_0' => $row[1],
                'sensor_1' => $row[2],
                'sensor_2' => $row[3],
                'sensor_3' => $row[4],
                'sensor_4' => $row[5],
                'sensor_5' => $row[6],
                'sensor_6' => $row[7],
                'sensor_7' => $row[8],
                'sensor_8' => $row[9],
                'sensor_9' => $row[10],
                'sensor_10' => $row[11],
                'sensor_11' => $row[12],
                'status_0' => $row[13],
                'status_1' => $row[14]
            ];
            $entity = $this->Data->patchEntity($entity, $data);
            $this->Data->save($entity);
        }
        
        fclose($handle);
        return true;
    }
}
